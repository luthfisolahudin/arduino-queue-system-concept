#define LED_PIN1 11
#define LED_PIN2 9
#define LED_PIN3 10

#define BUTTON_PIN1 2
#define BUTTON_PIN2 3
#define BUTTON_PIN3 4

#define PROCESS_EMPTY 0
#define PROCESS_CHECKING 1
#define PROCESS_PROCESSING 2
#define PROCESS_PAUSE 3

#define LED_CHECKING 10
#define LED_PROCESSING 255

#define CHECKING_DURATION 2500
#define PROCESSING_DURATION 4000
#define PAUSE_DURATION 500

typedef struct {
	unsigned long lastProcess;
	unsigned int lastProcessType;
} LEDInfo;

LEDInfo ledInfo1 = {0, PROCESS_EMPTY};
LEDInfo ledInfo2 = {0, PROCESS_EMPTY};
LEDInfo ledInfo3 = {0, PROCESS_EMPTY};

unsigned int queueSize = 0;

void setup() {
	pinMode(LED_PIN1, OUTPUT);
	pinMode(LED_PIN2, OUTPUT);
	pinMode(LED_PIN3, OUTPUT);

	pinMode(BUTTON_PIN1, INPUT_PULLUP);
	pinMode(BUTTON_PIN2, INPUT_PULLUP);
	pinMode(BUTTON_PIN3, INPUT_PULLUP);

	Serial.begin(9600);
}

// Digunakan untuk menghandle button ketika ditekan
void checkButton(int button) {
	// Jika button ditekan,
	// tambahkan antrean
	if (digitalRead(button) == LOW) {
		++queueSize;
		Serial.println("Item added to the queue. Queue size: " + String(queueSize));

		delay(500);  // Debouncing
	}
}

// Digunakan untuk memulai proses pada LED
bool startProcess(unsigned int ledPin, LEDInfo *info) {
	// Jika antrean kosong,
	// return false
	if (queueSize <= 0) {
		return false;
	}

	// Jika led tidak empty, alias tidak available untuk digunakan,
	// return false
	if ((*info).lastProcessType != PROCESS_EMPTY) {
		return false;
	}

	// Set tipe proses menjadi checking,
	// dan update last process menjadi saat ini
	(*info).lastProcessType = PROCESS_CHECKING;
	(*info).lastProcess = millis();

	--queueSize;

	analogWrite(ledPin, LED_CHECKING);

	Serial.println("Start processing queue item on LED " + String(ledPin));

	return true;
}

// Digunakan untuk menghandle checking,
// termasuk apakah checking sudah selesai atau masih berlangsung
bool continueChecking(unsigned int ledPin, LEDInfo *info) {
	// Jika LED bukan sedang proses checking,
	// return false
	if ((*info).lastProcessType != PROCESS_CHECKING) {
		return false;
	}

	// Jika checking sudah selesai,
	// lanjutkan menjadi processing
	if ((*info).lastProcess + CHECKING_DURATION <= millis()) {
		(*info).lastProcessType = PROCESS_PROCESSING;
	  analogWrite(ledPin, LED_PROCESSING);

		Serial.println("Continue processing queue item on LED " + String(ledPin));
	}

	return true;
}

// Digunakan untuk menghandle processing,
// termasuk apakah processing sudah selesai atau masih berlangsung
bool continueProcessing(unsigned int ledPin, LEDInfo *info) {
	// Jika LED bukan sedang processing,
	// return false
	if ((*info).lastProcessType != PROCESS_PROCESSING) {
		return false;
	}

	// Jika processing sudah selesai,
	// lanjutkan menjadi pause sebelum menghandle antrean yang lainnya
	if ((*info).lastProcess + PROCESSING_DURATION <= millis()) {
		(*info).lastProcessType = PROCESS_PAUSE;
		digitalWrite(ledPin, LOW);

		Serial.println("Finish processing queue item on LED " + String(ledPin));
	}

	return true;
}

// Digunakan untuk menghandle pause,
// termasuk apakah sudah bisa memproses antrean lain atau belum
bool checkPause(unsigned int ledPin, LEDInfo *info) {
	// Jika LED bukan sedang pause,
	// return false
	if ((*info).lastProcessType != PROCESS_PAUSE) {
		return false;
	}

	// Jika pause sudah selesai,
	// ubah menjadi empty
	if ((*info).lastProcess + PAUSE_DURATION <= millis()) {
		(*info).lastProcessType = PROCESS_EMPTY;

		Serial.println("LED " + String(ledPin) + " ready");
	}

	return true;
}

// Digunakan untuk menghandle antrean,
// dari awal proses hingga selesai
bool processQueue(unsigned int ledPin, LEDInfo *info) {
	switch ((*info).lastProcessType) {
		case PROCESS_EMPTY:
			if (queueSize >= 1) {
				return startProcess(ledPin, info);
			}

			break;
		case PROCESS_CHECKING:
			return continueChecking(ledPin, info);

		case PROCESS_PROCESSING:
			return continueProcessing(ledPin, info);

		case PROCESS_PAUSE:
			return checkPause(ledPin, info);

		default:
			break;
	}

	return false;
}

void loop() {
	checkButton(BUTTON_PIN1);
	checkButton(BUTTON_PIN2);
	checkButton(BUTTON_PIN3);

	processQueue(LED_PIN1, &ledInfo1);
	processQueue(LED_PIN2, &ledInfo2);
	processQueue(LED_PIN3, &ledInfo3);

	delay(100);  // Optional delay to reduce processing load
}
